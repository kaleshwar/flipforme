<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'devflipforme');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'vinove');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7_9KxCRDUgc:!{m.^=Pj0NF2<jTa[Z#v|O@Mbu+xZFCD-@H~)6 83hvbdwZip4PR');
define('SECURE_AUTH_KEY',  'H2<!vDl#6jc|s7p@/I]SLmz[6ueHub]@I6$He#9#&4x3gh>9V{:=6uJ| e/Hb[W+');
define('LOGGED_IN_KEY',    'ia//%^:4<}BM8]k)[`H84]}P_*4`&gFqXf uOMVkR#|yAA?>N%JfZ@RY <LDGSgk');
define('NONCE_KEY',        ',mkEv~7/$8d3.o+xRVV-{zlq00^&AZJv)2[|ge;Bwrwa,bi6# gIi.b^i-g)14*0');
define('AUTH_SALT',        'Tq]bt.Y{eZq*>o: 0y.l;-EIuPF@|09P);6u284zc5{P.|egoJ+D=9-!ZqS^YLN7');
define('SECURE_AUTH_SALT', 'o*?>vH(OvbNBw`h~APba|.({9s^J15F)b4S]^O9ZyOk#}SPP}|Tlh5sP/b2p@Ep)');
define('LOGGED_IN_SALT',   'ikXy%6X~28w~`Zt.r&U]?v7D3^ONM0N)xgS/(dkGUhMt2A7}4-{d3eu;BYb`/0~o');
define('NONCE_SALT',       '@;fH$;I-4nzjQgJLhksGCD2Q;_K9[w2udlmno@%s#D 1*9#CK>IbHv-_>&+-&h(i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD','direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
