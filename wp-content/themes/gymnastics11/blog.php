<?php
/**
 *  Template Name:Blog
 *
 * If the user has selected a static page for their upcoming event page, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner">
			<div class="row">
				<div class="col-sm-12">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</div>
</section><!-- banner end-->

<!-- blog -->
<section class="blog-main">
	<div class="container">
		<div class="blog-main-inner">			
			<div class="">
				<ul>
					 <?php
						$args = array( 'post_type' => 'post', 'posts_per_page' => -1 );
								$loop = new WP_Query( $args );
								while ( $loop->have_posts() ) : $loop->the_post();
									
					?>
						<li>
							<h3><?php the_title();?></h3>
																										 
							<p><?php echo the_field('bshort_description'); ?></p>

							<a href="<?php the_permalink();?>">read more</a>
						</li>

					<?php	endwhile; ?>
				</ul>
			</div> 
		</div>
	</div>
</section><!-- blog end-->	

<?php get_footer(); ?>
