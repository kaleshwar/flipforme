<!DOCTYPE html>
<html>
<head>
<script src="<?php echo get_template_directory_uri(); ?>/datatable/jquery-1.12.4.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/datatable/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/datatable/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/datatable/dataTables.bootstrap.min.css">

</head>
<body>
<h1>Trial Class Form </h1>
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
               <th>Parent’s Name</th>
                <th>Child’s Name</th>
                <th>Class</th>
                <th>DOB</th>
                <th>Age</th>
                <th>Start date</th>
                <th>E-mail </th>
                <th>View</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
               <th>Parent’s Name</th>
                <th>Child’s Name</th>
                <th>Class</th>
                <th>DOB</th>
                <th>Age</th>
                <th>Start date</th>
                <th>E-mail </th>
                <th>View</th>
            </tr>
        </tfoot>
        <tbody>
        <?php
         global $wpdb;
         $table_name = $wpdb->prefix . "trial_classform"; 
         $classformArr = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id DESC");
         if($classformArr){
            foreach($classformArr as $items){?>
              <tr>
                <td><?php echo $items->Parent_name; ?></td>
                <td><?php echo $items->Child1_name; ?></td>
                <td><?php echo $items->Class; ?></td>
                <td><?php echo $items->Child1_dob; ?></td>
                <td><?php echo $items->Child1_age; ?></td>
                <td><?php echo $items->Date; ?></td>
                <td><?php echo $items->Email; ?></td>
                <td><a href="admin.php?page=trial_manage&action=view&classID=<?php echo $items->id; ?>">View</a></td>

              </tr>

           <?php }
         }
         ?>
         </tbody>
    </table>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>