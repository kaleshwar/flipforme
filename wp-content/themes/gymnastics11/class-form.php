<?php
/**
 *  Template Name: classes form
 *
 * If the user has selected a static page for their classes page, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css"> 
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner">
			<div class="row">
				<div class="col-sm-12">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</div>
</section><!-- banner end -->



    <div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				
			<div class="container">	
				<form>
				<div class="form-cover full-width">
					<div class="full-width form-separter">
						<div class="text-center padding-medium">
							<h3>Flip For Me Gymnastics Registration, Waiver Application</h3>
						</div>
						<label class="full-width label-space">For Office Use Only</label>
						 <div class="col-md-4 col-xs-12" >
                                <label for="">Date</label>
                                   <div class="input-group date" id='datetimepicker1'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div> 
                             </div> 

						<div class="col-md-4 col-xs-12  form-group">
							<label for="">Class:</label>
							<input type="text" class="form-control" id="">
						</div>


						 <div class="col-md-4 col-xs-12" >
                                <label for="">Time</label>
                                   <div class="input-group date" id='datetimepicker1'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div> 
                             </div> 
					</div>

					<div class="full-width form-padding">
						<div class="col-md-4 col-xs-12 form-group">
							<label for="">Child's name:</label>
							<input type="text" class="form-control" id="">
						</div>

						 <div class="col-md-4 col-xs-12" >
                                <label for="">DOB</label>
                                   <div class="input-group date" id='datetimepicker1'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div> 
                             </div> 


						<div class="col-md-4 col-xs-12  form-group">
							<label for="">Age:</label>
							<input type="text" class="form-control" id="">
						</div>

							<div class="col-md-4 col-xs-12 form-group">
							<label for="">Child's name:</label>
							<input type="text" class="form-control" id="">
						</div>

						<div class="col-md-4 col-xs-12" >
                                <label for="">DOB</label>
                                   <div class="input-group date" id='datetimepicker1'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div> 
                             </div> 


						<div class="col-md-4 col-xs-12  form-group">
							<label for="">Age:</label>
							<input type="text" class="form-control" id="">
						</div>


						<div class="col-md-4 col-xs-12 form-group">
							<label for="">Child's name:</label>
							<input type="text" class="form-control" id="">
						</div>

						<div class="col-md-4 col-xs-12" >
                                <label for="">DOB</label>
                                   <div class="input-group date" id='datetimepicker1'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div> 
                             </div> 


						<div class="col-md-4 col-xs-12  form-group">
							<label for="">Age:</label>
							<input type="text" class="form-control" id="">
						</div>


					</div>


					

					<div class="full-width">
						<div class="col-md-4 col-xs-12 form-group">
							<label for="">Parents Name:</label>
							<input type="text" class="form-control input-styling" id="">
						</div>
						<div class="col-md-8 col-xs-12 form-group">
							<label for="">Address:</label>
							<input type="text" class="form-control input-styling" id="">
						</div>
					</div>

					<div class="full-width">
						
				

				
						<div class="col-md-4 col-xs-12 form-group">
							<label for="">City:</label>
							<input type="text" class="form-control input-styling" id="">
						</div>
					
						<div class="col-md-4 col-xs-12 form-group">
							<label for="">State:</label>
							<input type="text" class="form-control input-styling" id="">
						</div>

						<div class="col-md-4 col-xs-12 form-group">
							<label for="">zip:</label>
							<input type="text" class="form-control input-styling" id="">
						</div>

						<div class="col-md-4 col-xs-12 form-group">
							<label for="">Home Phone:</label>
							<input type="text" class="form-control input-styling" id="">
						</div>

						<div class="col-md-4 col-xs-12 form-group">
							<label for="">Cell Phone:</label>
							<input type="text" class="form-control input-styling" id="">
						</div>

						<div class="col-md-4 col-xs-12 form-group">
							<label for="">Email Address:</label>
							<input type="text" class="form-control input-styling" id="">
						</div>

						<div class="col-xs-12 form-group">
							<label for="">Any Medical Conditions:</label>
							<input type="text" class="form-control input-styling" id="">
						</div>

						<div class="col-xs-12 form-group">
							<label for="">How Did You Hear About Flip For Me Gymnastics</label>
							<input type="text" class="form-control input-styling" id="">
						</div>
                    </div>
                    

                    <div class="text-center full-width">
						  <button type="button" class="btn btn-default">Submit</button>

					</div>


					
				</div>
				</form>
			</div>

				
			</div>
		</div>

	</div>



<!-- classes -->

<!-- classes end -->

<script  src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script  src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

    
 <script type="text/javascript">
$(document).ready(function(){
   $(function () {
   var bindDatePicker = function() {
    $(".date").datetimepicker({
	
        format:'YYYY-MM-DD',
     	icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      }
    }).find('input:first').on("blur",function () {
      // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
      // update the format if it's yyyy-mm-dd
      var date = parseDate($(this).val());

      if (! isValidDate(date)) {
        //create date based on momentjs (we have that)
        date = moment().format('YYYY-MM-DD');
      }

      $(this).val(date);
    });
  }
   
   var isValidDate = function(value, format) {
    format = format || false;
    // lets parse the date to the best of our knowledge
    if (format) {
      value = parseDate(value);
    }
    var timestamp = Date.parse(value);
    return isNaN(timestamp) == false;
   }
   
   var parseDate = function(value) {
    var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
    if (m)
      value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

    return value;
   }
   
   bindDatePicker();
 });
 });
 </script>

<?php get_footer(); ?>
