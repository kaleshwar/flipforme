<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.2
 */

?>

<!-- Contact-main start-->
<section class="contact-main">
	<div class="map">
		<!-- <iframe src="<?php //the_field('map_url', 'option');?>"
		width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
       <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3226.267989485082!2d-115.22130000000001!3d36.038165!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xee9087e0efb9eb65!2sFlip+For+Me+Gymnastics!5e0!3m2!1sen!2sin!4v1525871227506" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<div class="contact-details">
		<div class="contact-details-inner">
			<ul>
				<li>
					<h3>
						<span class="icon"><img src="<?php the_field('heading_iconc', 'option');?>"></span>
						<?php the_field('locationheading', 'option');?>
						<span><?php the_field('locationa', 'option');?></span>
					</h3>

				</li>

				<li>
					<h3>
						<span class="icon"><img src="<?php the_field('phone_icon', 'option');?>"></span>
						<?php the_field('phone_heading', 'option');?>
						<span><?php the_field('phone_number', 'option');?></span>
					</h3>

				</li>

				<li>
					<h3>
						<span class="icon"><img src="<?php the_field('email_icon', 'option')?>"></span>
						<?php the_field('email_heading', 'option');?>
						<span>

							<?php the_field('email_address', 'option');?>
						</span>
					</h3>

				</li>
			</ul>
		</div>
	</div>
</section><!-- Contact-main end-->

<!-- Follow-us start-->
<section class="follow-us-main">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 aboutusP">
					<?php
// query for the about page
$your_query = new WP_Query('pagename=about-us');
while ($your_query->have_posts()): $your_query->the_post();?>
									           <div class="content">
											        <h3>
											        	<span> <img src="<?php the_field('heading_icon_small');?>" alt=""></span>
											        	<?php the_field('short_heading');?>
											        </h3>
											        <p><?php the_field('short_para');?></p>
											        <a href="<?php the_permalink(get_the_ID());?>"><?php the_field('read_more');?>
											        </a>
										      </div>

									   <?php endwhile;
wp_reset_postdata();?>
			</div>

			<div class="follow-us col-sm-6">
				<div class="followus-facebook">
					<?php dynamic_sidebar('Facebookfeed');?>
				</div>
				<div class="followus-twitter">
					<?php dynamic_sidebar('twitterfeed');?>
				</div>
			</div>
		</div>
	</div>
</section>	<!-- Follow-us end-->
<!-- Footer start-->
<footer class="footer">
	<div class="container">
		<div class="footer-inner">
			<div class="row">
				<div class="col-md-12 col-sm-12">
						<div class="footer-menu">
							<?php wp_nav_menu(array('theme_location' => 'footer'));?>
						</div>
				</div>
				<div class="col-md-12 col-sm-12">
					<div class="copyright">
						<?php dynamic_sidebar('Copyright');?>
					</div>

				</div>
			</div>
		</div>
	</div>
</footer><!-- Footer end-->

<script type="text/javascript">
    var $ = jQuery;
</script>
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>		 -->
<script type="text/javascript" src="<?php bloginfo('template_url')?>/assets/js/slick.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url')?>/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url')?>/assets/js/equal-height.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url')?>/assets/js/main.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	  $('.classes-type').slick({
	      infinite: true,
	      autoplay: true,
	      autoplaySpeed: 2000,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
			    {
			      breakpoint: 1025,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2,
			        infinite: true
			      }
			    },
			    {
			      breakpoint: 767,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			]
	  });
	  
	    $('.sliderImage').slick({
	      infinite: true,
	      autoplay: true,
	      autoplaySpeed: 2000,
		  slidesToShow: 4,
		  slidesToScroll: 4,
		  responsive: [
			    {
			      breakpoint: 1025,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2,
			        infinite: true
			      }
			    },
			    {
			      breakpoint: 767,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			]
	  });
	  

	  $('.banner').slick({
	      infinite: true,
	       autoplay: true,
	        autoplaySpeed: 2000,
		  slidesToShow: 1,
		  slidesToScroll: 1,

	  });
	});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var url = '<?php echo get_permalink(7) ?>';
        $('.woocommerce-error').find('.wc-forward').text('Continue shopping');
        $('.woocommerce-error').find('.wc-forward').attr('href',url);
    })

</script>
<?php wp_footer();?>

</body>
</html>
