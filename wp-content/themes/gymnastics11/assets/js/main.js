/* homepage Main Slider */
if(jQuery('#home_slider').length) {
    jQuery('#home_slider').owlCarousel({
        loop:true,
        margin:10,
        autoplay:true,
        nav: true,
        navText: [
            "<img src='http://devflipforme.n1.iworklab.com/wp-content/uploads/2017/09/icon-slider-left.png'>",
            "<img src='http://devflipforme.n1.iworklab.com/wp-content/uploads/2017/09/icon-slider-right.png'>"
        ],
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1,
                loop:true
            }
        }
    });
}

/*
$(document).ready(function() {
    // This will fire when document is ready:
    $(window).resize(function() {
        // This will fire each time the window is resized:
        if($(window).width() <= 767) {
            // if larger or equal
            $('.contact-form-main .form-inner').hasClass().removeClass('contact-box');
            alert('hello');
        } else {
            // if smaller
           $('.contact-form-main').addClass('contact-box');
        }
    }).resize(); // This will simulate a resize to trigger the initial run.
});


$(document).ready(function() {
    if($(window).width() <= 1024) {
         $('.upcoming-events-inner').removeClass('.form-inner');
         alert('hello');
    }
});*/

(function($) {
    var $window = $(window),
        $html = $('html');

    function resize() {
        if ($window.width() < 767) {
            return $html.removeClass('contact-box');
        }

        $html.addClass('contact-box');
    }    
    $window
        .resize(resize)
        .trigger('resize');
})(jQuery);