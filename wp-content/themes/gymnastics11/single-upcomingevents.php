<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php

			$eventImage =	get_field('event_image',get_the_ID());
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :					
				endif;

				// the_post_navigation( array(
				// ) );

			endwhile; // End of the loop.
			?>
			<!-- banner -->
			<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
				<div class="container">
					<div class="banner-inner">
						<div class="row">
							<div class="col-sm-12">
								<?php the_title( '<h1>', '</h1>' ); ?>
							</div>
						</div>
					</div> 
				</div>
			</section><!-- banner end -->

			<!-- event-details -->
			<div class="event-details-main">
				<div class="container">
					<div class="event-details-inner">
						<div class="row">
							<div class="col-sm-4">
								<img src="<?php echo $eventImage; ?>">
							</div>
							<div class="col-sm-8">
								<div class="content">
									<h3><?php echo get_field('event-detail_heading',get_the_ID()); ?></h3>
									<p><?php echo get_field('events_details_description',get_the_ID()); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- event-details end -->


		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();?>
