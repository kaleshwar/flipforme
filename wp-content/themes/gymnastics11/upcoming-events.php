<?php
/**
 *  Template Name:Upcoming Events
 *
 * If the user has selected a static page for their upcoming event page, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner">
			<div class="row">
				<div class="col-sm-12">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</div>
</section><!-- banner end-->

<!-- upcoming events -->
<section class="upcoming-events">
	<div class="container">
		<div class="upcoming-events-inner">
			<div class="row">
				<div class="col-sm-12">
					<h3 class="upcoming-heading"> <?php echo get_field('small_heading'); ?><span><?php echo get_field('big_heading'); ?></span>  </h3>


				</div>
			</div> 
			<div class="row">
				<div class="col-sm-12">
					<div class="event-listing">
						<p><?php echo get_field('events_para'); ?></p>
						<p><a href="#"><?php echo get_field('email_us'); ?></a> <?php echo get_field('email-us_para'); ?></p>
						<ul class="listing">
								
							<?php
									$my_query = new WP_Query('post_type=upcomingevents & showposts=-1&order=asc');
									while ($my_query->have_posts()) : $my_query->the_post();
									$post_id=$post->ID;
									$url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
									$imagecat_id = get_post_thumbnail_id($postid2);
									$imagecat_url = wp_get_attachment_image_src($imagecat_id,'full');
									$imagecat_url1 = $imagecat_url[0];
									$post_content=$post->post_content;
									$class = get_post_meta( $post_id,"other_class", true );
								?>
										
											<li class="box">
												<h3><?php echo get_field('event_heading'); ?></h3>
												<div class="img-box">
													<img src="<?php echo get_field('event_image');?>">

													<div class="event-details">
														<h3><?php echo get_field('event-detail_heading'); ?></h3>
														<p><?php echo get_field('events_details_description'); ?></p>
														<a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo get_field('event_details_view_more'); ?></a>
													</div>
												</div>
												<div class="button-list">
													<a href="#" class="viewBtn"><?php echo get_field('register_button'); ?></a>
													<a href="#" class="viewBtn addcart"><?php echo get_field('add_cart_button'); ?></a>
												</div>
											</li>

								<?php endwhile; wp_reset_query(); ?>
							</ul>
					</div>
   

					<div class="shop-list">
						<h3><?php echo get_field('flip_flop_shop_heading'); ?></h3>
						<p><a href="#"><?php echo get_field('emailus_flip'); ?></a> <?php echo get_field('callus_flip'); ?></p>
						<ul class="listing">
								
							<?php
									$my_query = new WP_Query('post_type=flipflopshop & showposts=-1&order=asc');
									while ($my_query->have_posts()) : $my_query->the_post();
									$post_id=$post->ID;
									$url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
									$imagecat_id = get_post_thumbnail_id($postid2);
									$imagecat_url = wp_get_attachment_image_src($imagecat_id,'full');
									$imagecat_url1 = $imagecat_url[0];
									$post_content=$post->post_content;
									$class = get_post_meta( $post_id,"other_class", true );
								?>
										
											<li class="box">
												<h3><?php echo get_field('flip_shop_heading'); ?></h3>
												<div class="img-box">
													<img src="<?php echo get_field('flip_shop_image');?>">

													
												</div>
												<div class="button-list">
													<a href="#" class="viewBtn"><?php echo get_field('flip_shop_registerbtn'); ?></a>
													<a href="#" class="viewBtn addcart"><?php echo get_field('flip_shop_addcartbtn'); ?></a>
												</div>
											</li>

								<?php endwhile; wp_reset_query(); ?>
							</ul>
					</div>
					</div>				
			</div>
		</div>
	</div>
</section><!-- upcoming events end-->	

<?php get_footer(); ?>
