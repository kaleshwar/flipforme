<?php
/**
 *  Template Name:About us
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner"> 
			<div class="row"> 
				<div class="col-sm-12"> 
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div> 
			</div>
		</div>
	</div>
</section><!-- banner end -->


<!-- contact form -->

<section class="upcoming-events aboutus">
	<div class="container">
		<div class="upcoming-events-inner">
			<div class="row">
				<div class="col-sm-12">
					<h3 class="upcoming-heading"> <?php echo get_field('about_small_heading'); ?><span><?php echo get_field('about_big_heading'); ?></span>  </h3>
					
					<div class="content-aboutus">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                                the_content();
                                endwhile; else: ?>
                            <?php endif; ?>
						
					</div>
			
					

				</div>
			</div> 
		</div>
	</div>
</section>


<?php get_footer(); ?>
