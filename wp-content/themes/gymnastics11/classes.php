<?php
/**
 *  Template Name: classes
 *
 * If the user has selected a static page for their classes page, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */
if(isset($_GET['add-to-cart'])){
        $url = get_permalink(456);
        wp_redirect($url);
        exit;
    }
get_header(); ?>

<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner">
			<div class="row">
				<div class="col-sm-12">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</div>
</section><!-- banner end -->

<!-- classes -->
<section class="upcoming-events">
	<div class="container">
		<div class="upcoming-events-inner">
			<div class="row">
				<div class="col-sm-12">
					<h3 class="upcoming-heading"> <?php echo get_field('classes_small_heading'); ?><span><?php echo get_field('classes_big_heading'); ?></span>  </h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="event-listing">
						<div class="term-para"><?php echo get_field('term_para'); ?></div>
						
						<div class="required-attire">
							<h3><?php echo get_field('required_attire_heading'); ?></h3>
							<p> <b><?php echo get_field('girls_heading'); ?></b><?php echo get_field('girls_description'); ?> </p>
							<p> <b><?php echo get_field('boy_heading'); ?></b><?php echo get_field('boy_description'); ?></p>
						</div>




						<div class="term-classes"> 
							<h3><?php echo get_term( 17, 'product_cat' )->name; ?></h3>
                              <ul class="listing">
                                <?php
                                    $args = array(
                                        'post_type' => 'product',
                                         'product_cat' => 'term-classes'
                                        );
                                    $loop = new WP_Query( $args );
                                    if ( $loop->have_posts() ) {
                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            wc_get_template_part( 'content', 'product' );
                                        endwhile;
                                    } else {
                                        echo __( 'No products found' );
                                    }
                                    wp_reset_postdata();
                                ?>
                            </ul><!--/.products-->
						</div>	


					   <div class="month-classes"> 
                            <h3><?php echo get_term( 18, 'product_cat' )->name; ?></h3>
                              <ul class="listing">
                                <?php
                                    $args = array(
                                        'post_type' => 'product',
                                         'product_cat' => 'month-classes'
                                        );
                                    $loop = new WP_Query( $args );
                                    if ( $loop->have_posts() ) {
                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            wc_get_template_part( 'content', 'product' );
                                        endwhile;
                                    } else {
                                        echo __( 'No products found' );
                                    }
                                    wp_reset_postdata();
                                ?>
                            </ul><!--/.products-->
                        </div>  				
					</div>				
				</div>				
			</div>
		</div>
	</div>
</section>	<!-- classes end -->


<?php get_footer(); ?>
