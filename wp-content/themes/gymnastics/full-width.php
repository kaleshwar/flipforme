<?php
/**
 *  Template Name:full width 
 * If the user has selected a static page for their upcoming event page, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php

while ( have_posts() ) : the_post(); 

    the_content();

endwhile; // End of the loop.
?>


<?php get_footer(); ?>
