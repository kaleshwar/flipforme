<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'gymnastics' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'gymnastics' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
