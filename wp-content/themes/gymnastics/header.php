<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress 
 * @subpackage Gymnastics_Theme
 * @since 1.0 
 * @version 1.0 
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/fonts.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/slick-theme.css">
<link rel="icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/images/favicon.png" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Header start-->
<header>
	<div class="top_header text-right">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-5 col-md-7 col-sm-12 col-xs-12">
					<p> 
						<span class="top_title">
							
							<?php the_field('top_header_title', 'option') ?>
						</span> 
						<!-- end of top title -->

						<span class="social_icons">
						<?php $args = array( 'post_type' => 'social_icons', 'posts_per_page' => 10, 'orderby'=>'date', 'order'=>'ASC' );
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post();
						?>
							
							<a href="<?php the_field('link') ?>"> <img src="<?php the_field('icon') ?>"> </a>

						<?php 		  
							endwhile;
							wp_reset_query();
						?>
						</span> 
						<!-- end of social icons -->

						<span class="call_us">
							<a href="tel:<?php the_field('call_us_number', 'option') ?>"> <img src="<?php the_field('call_us_icon', 'option') ?>"> <b> <?php the_field('call_us_number', 'option') ?> </b> </a>
						</span> 
						<!-- end of call us section --> 
					</p>
				</div>
			</div>
		</div>
	</div> <!-- end of top header -->

	<div class="main_header">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="brand_logo">
						<a href="<?php bloginfo('url') ?>"> 
							<img src="<?php the_field('brand_logo', 'option') ?>">
						</a>
					</div>
				</div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#brand_menu">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>                        
			    </button>

				<div class="col-md-11">
					<div class="brand_menu text-right" id="brand_menu">
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'my_extra_menu_class' ) ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</header><!-- Header end-->
