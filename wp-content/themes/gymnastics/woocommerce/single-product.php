<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<div class="wrap">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
 <?php

            $eventImage =   get_field('classes_imagem',get_the_ID());
            /* Start the Loop */
            while ( have_posts() ) : the_post();
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :                   
                endif;

                // the_post_navigation( array( 
                // ) );

            endwhile; // End of the loop.
            ?>
            <!-- banner -->
            <section class="banner-common" style="background-image: url(<?php the_field('banner_image','option'); ?> ">
                <div class="container">
                    <div class="banner-inner">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php the_title( '<h1>', '</h1>' ); ?>
                            </div>
                        </div>
                    </div> 
                </div>
            </section><!-- banner end -->



	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>

        </main><!-- #main -->
    </div><!-- #primary -->
</div><!--



/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
