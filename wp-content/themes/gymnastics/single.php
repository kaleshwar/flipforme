<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?> 

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

			

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					
				endif;

				

			endwhile; // End of the loop.
			?>



			<!-- banner -->
			<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
				<div class="container">
					<div class="banner-inner">
						<div class="row">
							<div class="col-sm-12">
								<?php the_title( '<h1>', '</h1>' ); ?>
							</div>
						</div>
					</div> 
				</div>
			</section><!-- banner end -->

			<!-- event-details -->
			<div class="event-details-main">
				<div class="container">
					<div class="event-details-inner">
						<div class="row">						
							<div class="col-sm-12 desp_class">
								<!-- <div class="content" style="width:800px;height: 450px;">
									
									<h3><?php the_title();?></h3>																										 
									<?php //the_content(); ?>
									<?php  //var_dump(get_the_ID());?>
									<img src="<?php  echo get_the_post_thumbnail_url(get_the_ID()); ?>">
									<?php //var_dump(get_the_post_thumbnail_url(get_the_ID())); ?>
									<?php //get_the_post_thumbnail(get_the_ID()); ?>


								</div> -->
								<h3><?php the_title();?></h3>	
								
							<div class="col-sm-3" style="padding-top:20px;">
								<img src="<?php  echo get_the_post_thumbnail_url(get_the_ID()); ?>" style="width: 300px;height: 300px;">
							</div>
							<div class="col-sm-9">
								<div class="content">
									
									<?php echo the_content(); ?>
								</div>
							</div>
						


							</div>
						</div>
					</div>
				</div>
			</div><!-- event-details end -->

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
