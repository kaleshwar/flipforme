<?php
/**
 *  Template Name:Team
 *
 * If the user has selected a static page for their upcoming event page, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner">
			<div class="row">
				<div class="col-sm-12">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</div>
</section><!-- banner end-->
  <?php
    while ( have_posts() ) : the_post(); 
    the_content();
    endwhile; // End of the loop.
  ?>
<!-- upcoming events -->



<?php get_footer(); ?>
