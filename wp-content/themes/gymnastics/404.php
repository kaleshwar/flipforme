<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>




<section class="error-404 not-found"> 
	<div class="container">					
		<div class="page-content">
			
			<img src="<?php bloginfo('template_url') ?>/assets/images/404-error.png">
			<h3>404 error</h3>
			<!-- <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'gymnastics' ); ?></p> -->
		</div><!-- .page-content -->
		<div class="search-box">
			<?php  get_search_form(); ?>
		</div>
	</div>
</section><!-- .error-404 -->

<?php get_footer();
