<?php
/**
 *  Template Name: TrialClassWaiverForm
 *
 * If the user has selected a static page for their classes page, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/flip-for-me/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/flip-for-me/css/style.css" type="text/css">

<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner">
			<div class="row">
				<div class="col-sm-12">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</div>
</section><!-- banner end -->


  <div class="container-fluid">
        <div class="row">
            <div class="container">
                
               <!--  <img src="<?php //echo get_template_directory_uri(); ?>/flip-for-me/images/logo.jpg" class="img-responsive center-block">
                 -->

                <?php 
                if(isset($_POST['trialform'])){
                 $formData = $_POST;
                 $Formobj = new TrialClass();
                 echo $Formobj->insertData($formData);
                } 
            ?>
                <form action="<?php echo get_permalink(get_the_ID()); ?>" method="post">
                <div class="col-md-12 frmmain">
                    <h3><span style="text-decoration:underline;">Flip For Me Gymnastics</span></h3>
                    <h5 class="text-center">Trial Class Form</h5>
                    
                    <p>For Office Use Only:</p>
                    
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Date </label>
                         <input type="date" name="addmission_date" value="" class="form-control" required>
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Class</label>
                         <input type="text" name="child_class" value="" class="form-control" required>
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Time</label>
                         <input type="time" name="addmission_time" value="" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    <hr style="border:2px dotted #d1d1d1; margin:50px 15px;">
                    
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Child’s Name </label>
                         <input type="text" name ="child0_name" value="" class="form-control" required>
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>DOB</label>
                         <input type="date" name ="child0_date" value="" class="form-control" required>
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Age</label>
                         <input type="number"  name ="child0_age" value="" class="form-control" required>
                        </div>
                    </div>
                    
                    
                    
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Child’s Name </label>
                         <input type="text" name ="child1_name" value="" class="form-control">
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>DOB</label>
                         <input type="date" name ="child1_dob" value="" class="form-control">
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Age</label>
                         <input type="number" name ="child1_age" value="" class="form-control">
                        </div>
                    </div>
                    
                    
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Child’s Name </label>
                         <input type="text" name="child2_name" value="" class="form-control">
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>DOB</label>
                         <input type="date" name="child2_dob" value="" class="form-control">
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Age</label>
                         <input type="number" name="child2_age" value="" class="form-control">
                        </div>
                    </div>
                    
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Parent’s Name(s)</label>
                         <input type="text" name="p_name" value="" class="form-control" required>
                        </div>
                    </div>
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Address
</label>
                         <input type="text" name="address" value="" class="form-control" required>
                        </div>
                    </div>
                    
                    
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>City
</label>
                         <input type="text" name="city" value="" class="form-control" required>
                        </div>
                    </div>
                    
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>State
</label>
                         <input type="text" name="state" value="" class="form-control" required>
                        </div>
                    </div>
                    
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Zip

</label>
                         <input type="text" name="zip" value="" class="form-control" required>
                        </div>
                    </div>
                    
                    
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Home Phone
</label>
                         <input type="number" name="home_phone" value="" class="form-control" required>
                        </div>
                    </div>
                    
                             
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Cell Phone
</label>
                         <input type="number" name="cell_phone" value="" class="form-control" required>
                        </div>
                    </div>
                    
                    
                    
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>E-mail Address
</label>
                         <input type="email" name="email_id" value="" class="form-control" required>
                        </div>
                    </div>
                    
                     
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Any medical conditions
</label>
                         <input type="text" name="medical_condition" value="" class="form-control" required>
                        </div>
                    </div>
                    
                    
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>How did you hear about Flip For Me Gymnastics?
</label>
                         <input type="text" name="about_gymnastics" value="" class="form-control" required>
                        </div>
                    </div>
                    
                    
                    <p>I am fully aware of and appreciate the risks of catastrophic injury, paralysis, and even death, as well as damages and losses,
associated with participation in gymnastics and other sports. I further agree that Flip For Me Gymnastics, it’s owners, employees,
staff, agents, and directors shall not be liable for any losses or damages occurring as a result of participation in a class or program. 
</p>
                    
                    <p>I herby give consent for Flip For Me Gymnastics to provide customary medical/athletics attention, transportation, and emergency
medical services as warranty in the course of my participation at Flip For Me Gymnastics. I maintain and uphold Primary Health
Insurance for my child and family who are participating at Flip For Me Gymnastics. </p>
                    
                    <h4>Blanket Waiver</h4>
                    <p>Due to insurance regulations, every person entering the main facility must read the following waiver and sign below as
an acknowledgement that he/she understands the following agreement. </p>
                    
                    <p>I acknowledge that by participating in gym activities and/or by moving around in the gym, with it’s equipment and
possible uneven surfaces, there is a risk of injury. I acknowledge that I accept the risk and waive the option to sue
should I or any minors for who I am responsible for, incur an injury. By waiving the option to sue, I also hereby release
Flip For Me Gymnastics and it’s agents or employees from liability for such injury. </p>
                    
                    
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group"> 
                           <label>Date

                           </label>
                         <input type="date" name="blanketwaive_date" value="" class="form-control" required>
                        </div>
                    </div>
                    
                    
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                        <label>Name of Parent/Guardian
                          </label>
                         <input type="text" name="parent_name" value="" class="form-control" required>
                        </div>
                    </div>

                  
                    <p>This form will be filed with my child’s registration form</p>
                     <div class="col-md-12 col-sm-12 col-xs-12 lasvegas">
                        <div class="form-group">
                              <input type="submit" name="trialform"  class="submit" value="SUBMIT">
                        </div>
                    </div>
                    
                    <p class="flipfor">Flip For Me Gymnastics<br>5905 West Wigwam Ave, Las Vegas, 89139 <br>(702) 202-0020 / WWW.FLIP4M.COM</p>
                    
                   </div>
                   <input type="hidden" name="action" value="newregistration" />
                </form>
            </div>
         </div>
    </div> 

<?php get_footer(); ?>
