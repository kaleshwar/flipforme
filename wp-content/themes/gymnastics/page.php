<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme 
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner"> 
			<div class="row"> 
				<div class="col-sm-12">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div> 
			</div>
		</div>
	</div>
</section><!-- banner end -->

<section class="main-content-default">
	<div class="container">

			<?php
			while ( have_posts() ) : the_post(); 

				the_content();

			endwhile; // End of the loop.
			?>
	</div><!-- #primary -->
</section><!-- .wrap -->

<?php get_footer();
