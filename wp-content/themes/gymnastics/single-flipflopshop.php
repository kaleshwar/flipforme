<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
<style>

.mfp-preloader {
  width: 30px;
  height: 30px;
  background-color: #FFF;
  opacity: 0.65;
  margin: 0 auto;
  -webkit-animation: rotateplane 1.2s infinite ease-in-out;
  animation: rotateplane 1.2s infinite ease-in-out;
}

@-webkit-keyframes rotateplane {
  0% { -webkit-transform: perspective(120px) }
  50% { -webkit-transform: perspective(120px) rotateY(180deg) }
  100% { -webkit-transform: perspective(120px) rotateY(180deg)  rotateX(180deg) }
}

@keyframes rotateplane {
  0% { transform: perspective(120px) rotateX(0deg) rotateY(0deg) }
  50% { transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg) }
  100% { transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg) }
}
</style>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			
			$eventImage =	get_field('flip_shop_image',get_the_ID());
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :					
				endif;

				// the_post_navigation( array(
				// ) );

			endwhile; // End of the loop.
			?>
			<!-- banner -->
			<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
				<div class="container">
					<div class="banner-inner">
						<div class="row">
							<div class="col-sm-12">
								<?php the_title( '<h1>', '</h1>' ); ?>
							</div>
						</div>
					</div> 
				</div>
			</section><!-- banner end -->

			<!-- event-details -->
			<div class="event-details-main">
				<div class="container">
					<div class="event-details-inner">
						<div class="row">
							<div class="col-sm-4" id="popup-gallery">
								<a href="<?php echo $eventImage; ?>"><img src="<?php echo $eventImage; ?>"></a>
							</div>
							<div class="col-sm-8">
								<div class="content">
									
									<?php echo the_content(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- event-details end -->


		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();?>
