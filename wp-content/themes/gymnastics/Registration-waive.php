<?php
/**
 * Template Name: Registration-waive
 *
 * If the user has selected a static page for their classes page, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/flip-for-me/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/flip-for-me/css/style.css" type="text/css">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/sign/jquery.signaturepad.css" rel="stylesheet">

<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner">
			<div class="row">
				<div class="col-sm-12">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</div>
</section><!-- banner end -->
	<div class="container-fluid">
        <div class="row">
            <div class="container">
                 <?php 
                  if(isset($_POST['submitval'])){
                   TrialClass::isSubmit($_POST);
                  }
                 ?>
                <form class="sign-reg-form" method="post">
                <?php //wp_nonce_field( 'regiform-class', 'regiform-class-nonce');?>
                <div class="col-md-12 frmmain">
                    <h3><span style="text-decoration:underline;">Flip For Me Gymnastics Registration, Waiver Application</span></h3>
                    <h4 style="margin:30px 0px;">Class Information: </h4>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Class Title </label>
                         <input type="text" name="class_name" class="form-control" required>
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Regular Class(es) – Date/Time</label>
                         <input type="text" name="regular_class" class="form-control" required>
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Tryout Class - Date/Time</label>
                         <input type="date" name="tryout_class" class="form-control" required>
                        </div>
                    </div>
                    
                    
                    
                     <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label>Start Date </label>
                         <input type="text" name="start_date" class="form-control" required>
                        </div>
                    </div>
                             <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label>Amount</label>
                         <input type="text" name="amount" class="form-control" required>
                        </div>
                    </div>
                    
                     <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label>Registration Fee </label>
                         <input type="text" name="re_start" class="form-control" required>
                        </div>
                    </div>
                    
                     <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label>Total Due  </label>
                         <input type="text" name="total_due" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                     <h4 style="margin:30px 0px;">Student Information: 
 </h4>
                    
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Students First and Last Name</label>
                         <input type="text" name="first_name" class="form-control" required>
                        </div>
                    </div>
                    
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Date of Birth</label>
                         <input type="date" name="dob" class="form-control" required>
                        </div>
                    </div>
                    
                    
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Address</label>
                         <input type="text" name="Address" class="form-control" required>
                        </div>
                    </div>
                    
                    
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>City
</label>
                         <input type="text" name="city" class="form-control" required>
                        </div>
                    </div>
                    
                    
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label>State</label>
                         <input type="text" name="state" class="form-control" required>
                        </div>
                    </div>
                    
                    
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label>Zip Code</label>
                         <input type="text" name="zipcode" class="form-control" required>
                        </div>
                    </div>
                    
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Email address:</label>
                         <input type="email" name="EmailId" class="form-control" placeholder="Please register your email address on our website: Flip4m.com" required>
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Parent /Guardian Name </label>
                         <input type="text" name="parent_name" class="form-control" required>
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Home Phone</label>
                         <input type="text" name="home_pone" class="form-control" required>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Cell Phone</label>
                         <input type="text" name="cell_phone" class="form-control" required>
                        </div>
                    </div>
                    
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Occupation</label>
                         <input type="text" name="occupation" class="form-control" required>
                        </div>
                    </div>
                    
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Work Phone</label>
                         <input type="text" name="workphone" class="form-control" required>
                        </div>
                    </div>
                    
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Alt. Phone</label>
                         <input type="text" name="alt_name" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Emergency Contact</label>
                         <input type="text" name="emergency_contact" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Phone</label>
                         <input type="text" name="phone" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Relationship to Student</label>
                         <input type="text" name="relation_student" class="form-control" required>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Allergies or Special Needs</label>
                         <input type="text" name="allergies" class="form-control" required>
                        </div>
                    </div>
                    
                    <p class="gymnast">Gymnasts should have a medical exam before participating in classes. Both the gymnastics and the
administrative staff should be made aware of any special needs your child may have. </p>
                    <h5>Waiver: </h5>
                    <p class="certify">I understand the nature of this activity and certify that my child is in good health, and in proper physical condition to participate in
gymnastics. I recognize that gymnastics involves height and motion which may create the possibility of injury. I acknowledge the
risks of this activity and allow, the above named, to participate in the activity. I hereby release “Flip For Me Gymnastics”, Inc. and its
employees from claims for injured which may be sustained while participating in any of our programs.</p>
                    
                    <p class="flipfor" style="text-align:left;">I understand that I am assuming all risks inherent in gymnastics known, or unknown, and am giving up my right to sue, <br>“Flip For Me Gymnastics”. </p>
                    

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Parent /Guardian Signature</label>

                            <div class="sigPad" id="smoothed" style="width:404px;">
                                <ul class="sigNav">
                                    <li class="clearButton"><a href="#clear">Clear</a></li>
                                </ul>
                                <div class="sig sigWrapper" style="height:auto;">
                                    <div class="typed"></div>
                                    <canvas class="pad" width="400" height="100"></canvas>
                                    <!-- <input type="hidden" name="output" class="output"> -->
                                </div>
                            </div>

                         <input type="hidden" name="guardian_signature" class="guardian-signature">
                        </div>
                    </div>
                    
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Date: </label>
                         <input type="date" name="date1" class="form-control" required>
                        </div>
                    </div>
                    
                      <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>5895 W. Wigwam Ave, Las Vegas, NV 89139</label>
                        
                        </div>
                    </div>
                    
                      <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>Office (702) 202-0020 </label>
                        
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>ver. 6/30/2015 P 1of4 </label>
                      
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    
                    <h3>Flip For Me Gymnastics Rules & Policies</h3>
                      <h4>Registration & Tuition </h4>
                    
                    <p>Initial <input type="text" name="tuition" class="withoutfrm" required>Each month\term tuition has 2 weeks pay period followed by <span style="text-decoration: underline;">late fee of $25.00 a</span>  as per Payment Schedule (see website) and the student will not be permitted to attend class until Full Payment is received. Monthly Tuition is constant, regardless of the number of
classes in each month. The *Parent on this registration form is responsible for all fees for the registered child. </p>
                    
                    <p>Initial  <input type="text" name="service_fee" class="withoutfrm" required> All checks returned by the bank for any reason will be charged a $35.00 service fee. </p>
                    <p>Initial  <input type="text" name="annual_fee" class="withoutfrm" required> An annual registration fee of $40.00\student or $50\family is due upon registering in any class, which is valid for 1 year
from the date of first class, regardless if the child attends every month. There’s no prorated refund on annual registration. </p>
                    
                    <p><b>Initial  <input type="text" name="refundable_fee" class="withoutfrm" required> All fees are non-refundable. Students with accounts that are past due will not be able to participate in class.  </b></p>
                    <p>Initial  <input type="text" name="credits_fee" class="withoutfrm" required> All terms are eight week terms. For new enrollments (Term or Month) tuition is paid in full plus registration. First invoice
will show prorated credits for free\missed classes if student did not start from beginning of Term or Month class. Any credits will apply to
next tuition which will be adjusted to bring balance to zero dollars. Continuing with 3rd Term or 3rd Month tuition, student should pay
tuition according to “Flip For Me” Payment Schedule. No refunds for any reason.
</p>
                    <p>Initial  <input type="text" name="month_fee" class="withoutfrm" required> “Flip For Me Gymnastics” runs monthly and/or 8 week sessions year round. New students are welcomed at any time.
Student may stop attending after first 8 weeks (Term tuition) or first 30 days (Month tuition), even if they started in middle Per Term or
Per Month tuition.
</p>
                    
                    
                     <h4>Registration & Tuition </h4>
                    <p>Initial  <input type="text" name="quality_program" class="withoutfrm" required>  In order to keep the quality of our programs high and the tuitions affordable, we do not allow refunds or credits for
                        missed classes, <b>for any reason.</b> Missed classes may be “made-up” by scheduling a make-up. Students may attend another class time in
their level based on availability. Parent should call in or email at least 1 hour prior date\time of regular missed class to announce absence
and to schedule for a make-up class. Absolutely NO schedule of “Make-up” to missed scheduled “Make-up”</p>
                    
                    <p>Initial  <input type="text" name="cost_program" class="withoutfrm" required>  <b>Missing a class does not lower the cost of running our programs; therefore, make-ups may not be used in place of
                        tuition.</b> Make ups are not transferable to another student and must be made in the Term that the class was missed. “Flip For Me
Gymnastics” reserves the right to limit class size and combine or close smaller classes. More details on “Missed class policy”
</p>
                    <p>Initial  <input type="text" name="scheduled" class="withoutfrm" required>  If student can not attend a scheduled class due to “Flip For Me” activity (Example: hosting a meet, closing gym for a
public holiday). Student may request a make-up class, prorated credit to account or refund (only for that class). Hosting meets are
usually on Saturdays and/or Sundays.
</p>
                      <h4>Registration & Tuition </h4>
                    <p>Initial  <input type="text" name="computer_office" class="withoutfrm" required> “Flip For Me Gymnastics” computer system automatically bills all past due accounts on the 7th of the month. Notifying
the office is required when your child is withdrawing from our program for any length of time, ie: one full month or permanently. You
will be billed until we receive this information. Any account left unpaid, the child will not be able to participate in class. </p>
                    
                    <h5>“Flip For Me Gymnastics” is not responsible for lost or stolen items. 
</h5>
                    <h6>I read and I fully understand the above terms of Registration and Tuition Policy
</h6>
                    
                    
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Full Name</label>
                         <input type="text" name="full_name" class="form-control" required>
                        </div>
                    </div>
                    
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Date: </label>
                         <input type="date" name="full_date" class="form-control" required>
                        </div>
                    </div>
                    
                    
                    
                            <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>5895 W. Wigwam Ave, Las Vegas, NV 89139</label>
                        
                        </div>
                    </div>
                    
                      <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>Office (702) 202-0020 </label>
                        
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>ver. 6/30/2015 P 2of4 </label>
                      
                        </div>
                    </div>
                    
                      <div class="clearfix"></div>
                    <hr>
                    
                    
                        <h3>Flip For Me Gymnastics </h3>
                    
                    <h5 class="agreement">RELEASE AND WAIVER OF LIABILITY, ASSUMPTION OF RISK, AND INDEMNITY AGREEMENT<br>(“AGREEMENT”) <br><span>Minor form </span> </h5>
                    
                    
                    <p class="flipfrmegym">In consideration of participating in any activity at<span style="text-decoration: underline;"> Flip For Me Gymnastics </span> I represent that I understand the nature of this Activity and
that I am qualified, in good health, and in proper physical condition to participate in such Activity. I acknowledge that if I believe event
conditions are unsafe, I will immediately discontinue participation in the activity. I fully understand that this Activity involves risks of
serious bodily injury, including permanent disability, paralysis and death, which may be caused by my own actions, or inactions, those
of others participating in the event, the conditions in which the event takes place, or the negligence of the “releasees” named below; and
that there may be other risks either not known to me or not readily foreseeable at this time; and I fully accept and assume all such risks
and all responsibility for losses, cost, and damages I incur as a result of my participation in the Activity. I hereby release, discharge, and
covenant not to sue <span style="text-decoration: underline;"> Flip For Me Gymnastics </span> , its respective administrators, directors, agents, officers, volunteers, and employees, other
participants, any sponsors, advertisers, and, if applicable, owners and lessors of premises on which the Activity takes place, (each
considered one of the “RELEASEES” herein) from all liability, claims, demands, losses, or damages, on my account caused or alleged to
be caused in whole or in part by the negligence of the “releasees” or otherwise, including negligent rescue operations and future agree
that if, despite this release, waiver of liability, and assumption of risk I, or anyone on my behalf, makes a claim against any of the
Releasees, I will indemnify, save, and hold harmless each of the Releasees from any loss, liability, damage, or cost, which any may incur
as the result of such claim. I have read the RELEASE AND WAIVER OF LIABILITY, ASSUMPTION OF RISK, AND INDEMNITY
AGREEMENT, understand that I have given up substantial rights by signing it and have signed it freely and without any inducement or
assurance of any nature and intend it to be a complete and unconditional release of all liability to the greatest extent allowed by law and
agree that if any portion of this agreement is held to be invalid the balance, notwithstanding, shall continue in full force and effect.</p>
                    
               <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Full Name</label>
                         <input type="text" name="partnership_name" class="form-control" placeholder="Printed name of participant 
" required>
                        </div>
                    </div>
                    
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Date: </label>
                         <input type="date" name="full_date" class="form-control" required>
                        </div>
                    </div>
            
            
                    <p class="parental">*PARENTAL CONSENT AND I, the minor’s *parent and/or legal guardian, understand the nature of the above referenced activities
and the Minor’s experience and capabilities and believe the minor to be qualified to participate in such activity. I hereby Release,
discharge, covenant not to sue and AGREE TO INDEMNIFY AND SAVE AND HOLD HARMLESS each of the Releasees from all
liability, claims, demands, losses or damages on the minor’s account caused or alleged to have been caused in whole or in part by the
negligence of the Releasees or otherwise, including negligent rescue operations, and further agree that if, despite this release, I, the
minor, or anyone on the minor’s behalf makes a claim against any of the above Releasees, I WILL INDEMNIFY, SAVE AND HOLD
HARMLESS each of the Releases from any litigation expenses, attorney fees, loss liability, damage, or cost any Releasee may incur as
the result of any such claim.</p>
                    
                    <p class="parental">My or my child's photo\video may be used in promotional or display material that may appear in the gym, website or local
publications. I will not hold “Flip For Me Gymnastics” liable for any issues that arise due to this photo/video being used. I
understand that if requested, the photo/video will be removed immediately. If I provide an email address on this registration, I
agree to receive occasional emails from “Flip For Me Gymnastics” with activities and news (Email form does have an unsubscribe
button to remove your address from receiving future email messages).</p>
                    
                    
                    
                                        
               <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Printed Name of *Parent/or Legal Guardian
</label>
                         <input type="text" name="printed_name" class="form-control" required>
                        </div>
                    </div>
                    
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Date: </label>
                         <input type="date" name="printed_date" class="form-control" required>
                        </div>
                    </div>
                    
                         <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>5895 W. Wigwam Ave, Las Vegas, NV 89139</label>
                        
                        </div>
                    </div>
                    
                      <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>Office (702) 202-0020 </label>
                        
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>ver. 6/30/2015 P 3of4 </label>
                      
                        </div>
                    </div>
                    
                      <div class="clearfix"></div>
                    <hr>
                    
                    
                      <h3>Flip For Me Gymnastics </h3>
                    
                    <h5 class="agreement">RELEASE AND WAIVER OF LIABILITY, ASSUMPTION OF RISK, AND INDEMNITY AGREEMENT 
<br>(“AGREEMENT”) <br><span>* Parent Participant form  </span> </h5>
                    
                    <p>In consideration of participating in any activity at <span style="text-decoration: underline;">Flip For Me Gymnastics</span> I represent that I understand the nature of this Activity and
that I am qualified, in good health, and in proper physical condition to participate in such Activity. I acknowledge that if I believe event
conditions are unsafe, I will immediately discontinue participation in the Activity. I fully understand that this Activity involves risks of
serious bodily injury, including permanent disability, paralysis and death, which may be caused by my own actions, or inactions, those
of others participating in the event, the conditions in which the event takes place, or the negligence of the “releasees” named below; and
that there may be other risks either not known to me or not readily foreseeable at this time; and I fully accept and assume all such risks
and all responsibility for losses, cost, and damages I incur as a result of my participation in the Activity. I hereby release, discharge, and
covenant not to sue <span style="text-decoration: underline;">Flip For Me Gymnastics</span>, its respective administrators, directors, agents, officers, volunteers, and employees, other
participants, any sponsors, advertisers, and, if applicable, owners and lessors of premises on which the Activity takes place, (each
considered one of the “RELEASEES” herein) from all liability, claims, demands, losses, or damages, on my account caused or alleged to
be caused in whole or in part by the negligence of the “releasees” or otherwise, including negligent rescue operations and future agree
that if, despite this release, waiver of liability, and assumption of risk I, or anyone on my behalf, makes a claim against any of the
Releasees, I will indemnify, save, and hold harmless each of the Releasees from any loss, liability, damage, or cost, which any may incur
as the result of such claim. I have read the RELEASE AND WAIVER OF LIABILITY, ASSUMPTION OF RISK, AND INDEMNITY
AGREEMENT, understand that I have given up substantial rights by signing it and have signed it freely and without any inducement or
assurance of any nature and intend it to be a complete and unconditional release of all liability to the greatest extent allowed by law and
agree that if any portion of this agreement is held to be invalid the balance, notwithstanding, shall continue in full force and effect. </p>
                    
                    
                                              
               <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Printed name of participant 
</label>
                         <input type="text" name="p_participant" class="form-control"  required>
                        </div>
                    </div>
                    
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Date: </label>
                         <input type="date"  name ="party_date" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    
                    <p class="parentalorle">* Parent or Legal Guardian or Sibling or Caregiver or Adult Participant 
</p>
                    
                    
                    
                        <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>5895 W. Wigwam Ave, Las Vegas, NV 89139</label>
                        
                        </div>
                    </div>
                    
                      <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>Office (702) 202-0020 </label>
                        
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                            <label>ver. 6/30/2015 P 4of4 </label>
                      
                        </div>
                    </div>
                    
                    
                     <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                              <input type="submit" name="submitval" class="submit" value="SUBMIT">
                        </div>
                    </div>
                    
                </div>
                </form>
            </div>
        </div>
    </div>

<?php get_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sign/numeric-1.2.6.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sign/bezier.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sign/jquery.signaturepad.min.js"></script>

<script type="text/javascript">
    var sign;
    jQuery(document).ready(function() {
        sign = jQuery('#smoothed').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:220});
        
        jQuery('.sign-reg-form').on('submit', function() {
            jQuery('.guardian-signature').val( sign.getSignatureImage() );
            // console.log( jQuery('.guardian-signature').val() );
            // return false;
        });
    });
</script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sign/json2.min.js"></script>