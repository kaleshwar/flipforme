 <html>
 <head>
 <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/flip-for-me/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/flip-for-me/css/style.css" type="text/css">
<script>
function myFunction() {
    window.print();
}
</script>
 <style type="text/css">

    #printable { display: none; }

    @media print
    {
        #adminmenumain { display: none; }
        .notice.notice-warning {
            display: none;
        }
        #printable { display: block; }
    }
    </style>
 </head>
<body>
  <div class="container-fluid">
        <div class="row">
            <div class="container">
                
           
                <div class="col-md-12 frmmain">
                    <h3><span style="text-decoration:underline;"><strong>Flip For Me Gymnastics</strong></span></h3>
                    <h5 class="text-center">Trial Class Form</h5>
                    
                    <p><strong>For Office Use Only:</strong></p>
                    <?php global $wpdb;
                      $userid = $_REQUEST['classID'];
                      $table_name = $wpdb->prefix . "trial_classform";
                      $studentArr = $wpdb->get_row("SELECT * FROM $table_name WHERE id = $userid");
                      // echo'<pre>';
                      // print_r($studentArr);
                      // echo'</pre>';
                      ?>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong> Date : </strong> </label>
                          <?php echo $studentArr->Date; ?>
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong>Class : </strong></label>
                            <?php echo $studentArr->Class; ?>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong>Time : </strong></label>
                         <?php echo $studentArr->Time_date; ?>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    <hr style="border:2px dotted #d1d1d1; margin:50px 15px;">
                    
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong>Child’s Name : </strong></label>
                        <?php echo $studentArr->Child1_name; ?>
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong>DOB : </strong></label>
                        <?php echo $studentArr->Child1_dob; ?>
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong>Age : </strong></label>
                         <?php echo $studentArr->Child1_age; ?>
                        </div>
                    </div>
                    
                    
                    
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong>Child’s Name  : </strong></label>
                         <?php echo $studentArr->Child2_name; ?>
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong>DOB : </strong></label>
                         <?php echo $studentArr->Child2_dob; ?>
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong>Age : </strong></label>
                         <?php echo $studentArr->Child2_age; ?>
                        </div>
                    </div>
                    
                    
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong> Child’s Name : </strong> </label>
                         <?php echo $studentArr->Child3_name; ?>                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong> DOB : </strong></label>
                       <?php echo $studentArr->Child3_dob; ?> 
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong> Age : </strong></label>
                         <?php echo $studentArr->Child3_age; ?> 
                        </div>
                    </div>
                    
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label> <strong> Parent’s Name(s) :</strong></label>
                         <?php echo $studentArr->Parent_name; ?> 
                        </div>
                    </div>
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label><strong> Address : </strong>
                      </label>
                         <?php echo $studentArr->Address; ?>
                        </div>
                    </div>
                    
                    
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label> <strong> City: </strong>
                         </label>
                        <?php echo $studentArr->City; ?>
                        </div>
                    </div>
                    
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label><strong> State : </strong>
                          </label>
                         <?php echo $studentArr->state; ?>
                        </div>
                    </div>
                    
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label> <strong> Zip : </strong>

                          </label>
                         <?php echo $studentArr->zip; ?>
                        </div>
                    </div>
                    
                    
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label> <strong> Home Phone : </strong>
                         </label>
                         <?php echo $studentArr->Home_phone; ?>
                        </div>
                    </div>
                    
                             
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label> <strong> Cell Phone : </strong> 
                           </label>
                         <?php echo $studentArr->Cell_phone; ?>
                        </div>
                    </div>
                    
                    
                    
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label><strong> E-mail Address : </strong>
                          </label>
                         <?php echo $studentArr->Email; ?>
                        </div>
                    </div>
                    
                     
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label><strong>Any medical conditions </strong> : 
                           </label> <label><?php echo $studentArr->medical_condition; ?></label>
                         
                        </div>
                    </div>
                    
                    
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <h4>How did you hear about Flip For Me Gymnastics? : 
                           </h4>
                        <label><?php echo $studentArr->medical_condition; ?></label>
                        </div>
                    </div>
                    
                    
                    <p>I am fully aware of and appreciate the risks of catastrophic injury, paralysis, and even death, as well as damages and losses,
associated with participation in gymnastics and other sports. I further agree that Flip For Me Gymnastics, it’s o
</p>
                    
                    <p>I herby give consent for Flip For Me Gymnastics to provide customary medical/athletics attention, transportation, and emergency
medical services as warranty in the course of my participation at Flip For Me Gymnastics. I maintain and uphold Primary Health
Insurance for my child and family who are participating at Flip For Me Gymnastics. </p>
                    
                    <strong>Blanket Waiver</strong>
                    <p>Due to insurance regulations, every person entering the main facility must read the following waiver and sign below as
an acknowledgement that he/she understands the following agreement. </p>
                    
                    <p>I acknowledge that by participating in gym activities and/or by moving around in the gym, with it’s equipment and
possible uneven surfaces, there is a risk of injury. I acknowledge that I accept the risk and waive the option to sue
should I or any minors for who I am responsible for, incur an injury. By waiving the option to sue, I also hereby release
Flip For Me Gymnastics and it’s agents or employees from liability for such injury. </p>
                    
                    
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group"> 
                          <strong>Date </strong> : 
                         <label><?php echo $studentArr->bank_date; ?></label>
                        </div>
                    </div>
                    
                    
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                        <label><strong>Name of Parent/Guardian</strong> : 
                          </label>
                         <label><?php echo $studentArr->Guardian; ?></label>
                        </div>
                    </div>

                  
                   <button onclick="myFunction()">Print this Page</button>
                    
                   </div>
                 
            </div>
         </div>
    </div> 
    </body>
    </html>