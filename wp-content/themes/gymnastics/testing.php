<?php
/**
 * Template Name:testing
 *
 * If the user has selected a static page for their classes page, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Gymnastics_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/flip-for-me/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/flip-for-me/css/style.css" type="text/css">

<!-- banner -->
<section class="banner-common" style="background-image: url(<?php the_field('banner_image'); ?> ">
	<div class="container">
		<div class="banner-inner">
			<div class="row">
				<div class="col-sm-12">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</div>
</section><!-- banner end -->
	<div class="container-fluid">
        <div class="row">
            <div class="container">
               <?php if(isset($_POST['submitval'])){
                        $to = 'kaleshwar.yadav@mail.vinove.com';
                        $subject = 'Trial Class Form';
                        $admin_email = get_option( 'admin_email' );
                        $sender = get_bloginfo( 'name' );
                        //$message = 'Your new password is: '.$random_password;
                        $headers[] = 'MIME-Version: 1.0' . "\r\n";
                        $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        $headers[] = "X-Mailer: PHP \r\n";
                        $headers[] = 'From:Heybot <'.$admin_email.'>' . "\r\n";
                        $message = '<!DOCTYPE html>
                        <html>
                        <head>
                        <title>HTML email</title>
                        </head>
                        <body>
                        <p>This email contains HTML Tags!</p>
                        <table>
                        <tr>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        </tr>
                        <tr>
                        <td>John</td>
                        <td>Doe</td>
                        </tr>
                        </table>
                        </body>
                        </html>';
                      $mail = wp_mail($to, $subject, $message, $headers);
                       if($mail){ echo"mail sent"; }

                   } 
                ?>
               
                <form action="#" method="post">
                <?php //wp_nonce_field( 'regiform-class', 'regiform-class-nonce');?>
                <div class="col-md-12 frmmain">
                    <h3><span style="text-decoration:underline;">Flip For Me Gymnastics Registration, Waiver Application</span></h3>
                    <h4 style="margin:30px 0px;">Class Information: </h4>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Class Title </label>
                         <input type="text" name="class_name" class="form-control" required>
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Regular Class(es) – Date/Time</label>
                         <input type="text" name="regular_class" class="form-control" required>
                        </div>
                    </div>
                    
                             <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Tryout Class - Date/Time</label>
                         <input type="date" name="tryout_class" class="form-control" required>
                        </div>
                    </div>
                    
                    
                     <div class="col-md-4 col-sm-4 col-xs-12 lasvegas">
                        <div class="form-group">
                              <input type="submit" name="submitval" class="submit" value="SUBMIT">
                        </div>
                    </div>
                    
                </div>
                </form>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
