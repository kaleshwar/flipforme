<?php
//Shortcodes mapping in Visual Composer
add_action( 'vc_before_init', 'map_shortcodes' );

function map_shortcodes()
{
 vc_map( array(
  'name'     => __('Photo slider'),
  'base'     => 'photo_slider',
  'params'   => array(
   array(
        "type" => "textfield",
        "heading" => __( "Heading", "flip4me" ),
        "param_name" => "heading",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
      'type'        => 'dropdown',
      'heading'     => __('Do you want to show slider yes/no'),
      'param_name'  => 'slider',
      'admin_label' => true,
      'value'       => array(
        'yes'   => 'yes',
        'no'   => 'no'
      ),
      ),
    array(
        "type" => "textfield",
        "heading" => __( "view url", "flip4me" ),
        "param_name" => "url",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    )
  )
);


vc_map( array(
  'name'     => __('GYMNASTICS TEAM'),
  'base'     => 'gymnastics_team',
  'params'   => array(
   array(
        "type" => "textfield",
        "heading" => __( "Heading", "flip4me" ),
        "param_name" => "heading",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
        "type" => "textfield",
        "heading" => __( "Sub Heading", "flip4me" ),
        "param_name" => "subheading",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
      "type" => "exploded_textarea",
      "heading" => __( "Description", "flip4me" ),
      "param_name" => "des",
      "value" => '', 
      "description" => __( "Enter description.", "flip4me" )
    ),
    array(
     "type" => "attach_images",
     "class" => "",
     "heading" => __( "Field Label", "my-text-domain" ),
     "param_name" => "field_name",
     "value" => '',
     "description" => __( "Enter description.", "my-text-domain" )
)
    )
  )
);

vc_map( array(
  'name'     => __('Get In Touch Form'),
  'base'     => 'get_in_touch_form',
  'params'   => array(
   array(
        "type" => "textfield",
        "heading" => __( "Heading", "flip4me" ),
        "param_name" => "heading",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
        "type" => "textfield",
        "heading" => __( "Sub Heading", "flip4me" ),
        "param_name" => "subhead",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
      "type" => "exploded_textarea",
      "heading" => __( "Description", "flip4me" ),
      "param_name" => "desc",
      "value" => '', 
      "description" => __( "Enter description.", "flip4me" )
    ),
    array(
        "type" => "textfield",
        "heading" => __( "Paypal Link", "flip4me" ),
        "param_name" => "paypal",
        "value" => __( "", "flip4me" ),
        "description" => __( "Paypal secure link here", "flip4me" )
         ),
    array(
        "type" => "textfield",
        "heading" => __( "Paypal description", "flip4me" ),
        "param_name" => "paypaldesc",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
  
    )
  )
);


vc_map( array(
  'name'     => __('Home page slider'),
  'base'     => 'home_page_slider',
  'params'   => array(
     array(
      'type'        => 'dropdown',
      'heading'     => __('Do you want to show slider yes/no'),
      'param_name'  => 'home_slider',
      'admin_label' => true,
      'value'       => array(
        'yes'   => 'yes',
        'no'   => 'no'
      ),
      )
  
    )
  )
);

vc_map( array(
  'name'     => __('Testimonial'),
  'base'     => 'testimonial_list',
  'params'   => array(
     array(
      'type'        => 'dropdown',
      'heading'     => __('Do you want to show Testimonial yes/no'),
      'param_name'  => 'show_testimonial',
      'admin_label' => true,
      'value'       => array(
        'yes'   => 'yes',
        'no'   => 'no'
      ),
      )
  
    )
  )
);


vc_map( array(
  'name'     => __('Our Class slider'),
  'base'     => 'our_class_room',
  'params'   => array(
   array(
        "type" => "textfield",
        "heading" => __( "Our Class Title", "flip4me" ),
        "param_name" => "heading",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
        "type" => "textfield",
        "heading" => __( "view all url", "flip4me" ),
        "param_name" => "url",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
        "type" => "colorpicker",
        "class" => "",
        "heading" => __( "Text color", "my-text-domain" ),
        "param_name" => "color",
        "value" => '#FF0000', //Default Red color
        "description" => __( "Choose text color", "my-text-domain" )
         ),

    )
  )
);

vc_map( array(
  'name'     => __('About flip for me'),
  'base'     => 'about_flip4me',
  'params'   => array(
   array(
        "type" => "attach_image",
        "heading" => __( "Heading icon left", "flip4me" ),
        "param_name" => "icon",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
        "type" => "textfield",
        "heading" => __( "Heading Text", "flip4me" ),
        "param_name" => "headingtext",
        "value" => '', 
        "description" => __( "", "flip4me" )
         ),
    array(
        "type" => "textfield",
        "class" => "",
        "heading" => __( "About heading", "flip4me" ),
        "param_name" => "abouttext",
        "value" => '', 
        "description" => __( "Type text here", "flip4me" )
         ),
      array(
         "type" => "exploded_textarea",
         "heading" => __( "Description", "flip4me" ),
         "param_name" => "desc",
         "value" => '', 
         "description" => __( "Enter description.", "flip4me" )
        ),
     array(
        "type" => "textfield",
        "heading" => __( "Learn more", "flip4me" ),
        "param_name" => "learnmore",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
     array(
        "type" => "attach_image",
        "heading" => __( "Heading icon right", "flip4me" ),
        "param_name" => "iconright",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
        "type" => "textfield",
        "heading" => __( "Heading Text", "flip4me" ),
        "param_name" => "headingright",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
    array(
        "type" => "textfield",
        "class" => "",
        "heading" => __( "About heading", "flip4me" ),
        "param_name" => "aboutright",
        "value" => '', //Default Red color
        "description" => __( "Type text here", "my-text-domain" )
         ),
      array(
         "type" => "exploded_textarea",
         "heading" => __( "Description", "flip4me" ),
         "param_name" => "descright",
         "value" => '', 
         "description" => __( "Enter description.", "flip4me" )
        ),
     array(
        "type" => "textfield",
        "heading" => __( "Learn more", "flip4me" ),
        "param_name" => "learnmoreright",
        "value" => __( "", "flip4me" ),
        "description" => __( "", "flip4me" )
         ),
  
    )
  )
);
}

