<!DOCTYPE html>
<html>
<head>
<script src="<?php echo get_template_directory_uri(); ?>/datatable/jquery-1.12.4.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/datatable/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/datatable/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/datatable/dataTables.bootstrap.min.css">

</head>
<body>
<h1>REGISTRATION WAIVE LISTS</h1>
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Class</th>
                <th>City</th>
                <th>State</th>
                <th>E-mail</th>
                <th>Amount</th>
                <th>Parent Name</th>
                <th>Start date</th>
                <th>View</th>
                
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Class</th>
                <th>City</th>
                <th>State</th>
                <th>State</th>
                <th>Amount</th>
                <th>Parent Name</th>
                <th>Start date</th>
                <th>View</th>
            </tr>
        </tfoot>
        <tbody>
             <?php
         global $wpdb;
         $table_name = $wpdb->prefix . "waiverregistration"; 
         $classformArr = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id DESC");
         if($classformArr){
            foreach($classformArr as $items){?>
              <tr>
                <td><?php echo $items->first_name; ?></td>
                <td><?php echo $items->class_name; ?></td>
                <td><?php echo $items->city; ?></td>
                <td><?php echo $items->state; ?></td>
                <td><?php echo $items->EmailId; ?></td>
                <td><?php echo $items->amount; ?></td>
                <td><?php echo $items->parent_name; ?></td>
                <td><?php echo $items->start_date; ?></td>
                <td><a href="admin.php?page=rg_waiver&action=wview&wid=<?php echo $items->id; ?>">View</a></td>
              </tr>

           <?php }
         }
         ?>
            

        </tbody>
    </table>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>