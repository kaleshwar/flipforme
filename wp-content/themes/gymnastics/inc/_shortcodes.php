<?php
/********  Shortcodes ********************************************************/
add_shortcode('photo_slider','photo_slider');  	   
function photo_slider($atts,$content,$tag)
{
    $a = shortcode_atts(array(
        'heading' => '',
        'slider' => '',
        'url' => '',
        ),$atts);
       ob_start();
       $postid=19;
?>
 <!-- Gallery start-->
<!-- Gallery start-->
<section class="gallery">
        <div class="gallery-inner">
            <div class="heading">
                <h3><?php echo $a['heading']; ?></h3>
            </div>
            <ul class="sliderImage" >               
                <?php
                    $my_query = new WP_Query('post_type=gallery & showposts=-1&order=asc');
                    while ($my_query->have_posts()) : $my_query->the_post();
                    $post_id=$post->ID;
                    $url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
                    $imagecat_id = get_post_thumbnail_id($postid2);
                    $imagecat_url = wp_get_attachment_image_src($imagecat_id,'full');
                    $imagecat_url1 = $imagecat_url[0];
                    $post_content=$post->post_content;
                    $class = get_post_meta( $post_id,"other_class", true );
                ?>
                        
                    <li>                        
                        <div class="img-box">
                            <img src="<?php echo get_field('gallery_image');?>">                                                
                        </div>                              
                    </li>
                                                
                <?php endwhile; wp_reset_query(); ?>
            </ul>
            <?php if(get_the_ID()!=$postid): ?>
            <div class="view-button">
                <a href="<?php echo $a['url']; ?>" target="_blank" class="viewBtn"> View All</a>
            </div>
        <?php endif ?>
       </div>
</section><!-- Gallery end-->

    <?php
    return ob_get_clean();
}
add_shortcode('gymnastics_team','gymnastics_team'); 
function gymnastics_team($atts,$content,$tag){

   $attr = shortcode_atts(array(
        'heading' => '',
        'subheading'=>'',
        'des' => '',
        'field_name'=>'',
        ),$atts);
        ob_start();
        $imgIDs = explode(',',$atts['field_name']);
    ?>
    <section class="upcoming-events team-member-main">
    <div class="container">
        <div class="upcoming-events-inner">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="upcoming-heading"><?php echo $attr['heading']; ?><span><?php echo $attr['subheading']; ?></span>  </h3>

                    <p class="cpara"><?php echo $attr['des']; ?></p>
                </div>
            </div> 
            <div class="row">
                <div class="col-sm-12">
                    <div class="event-listing">
                        <p><?php echo get_field('events_para'); ?></p>
                        <p><a href="#"><?php echo get_field('email_us'); ?></a> <?php echo get_field('email-us_para');?></p>
                        <ul class="listing">
                        <?php
                        $my_query = new WP_Query('post_type=teammember & showposts=-1&order=asc');
                        while ($my_query->have_posts()) : $my_query->the_post();
                        $post_id=$post->ID;
                        $url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
                        $imagecat_id = get_post_thumbnail_id($postid2);
                        $imagecat_url = wp_get_attachment_image_src($imagecat_id,'full');
                        $imagecat_url1 = $imagecat_url[0];
                        $post_content=$post->post_content; 
                        $class = get_post_meta( $post_id,"other_class", true );  
                        ?>
                        <li class="box">
                            <div class="img-box">
                                <img src="<?php echo get_field('team_member_image',get_the_ID());?>">                                                    
                            </div>
                            <div class="about-member">
                                <div class="member-name-main">
                                    <a href="<?php echo get_permalink(get_the_ID()); ?>" class="member-name"><?php echo get_field('team_member_name',get_the_ID()); ?></a>
                                </div>
                                <div class="member-position-main">
                                    <a href="<?php echo get_permalink(get_the_ID()); ?>" class="member-position"><?php echo get_field('team_member_position',get_the_ID()); ?></a>
                                </div>
                            </div>
                        </li>

                        <?php endwhile; wp_reset_query(); ?>
                    </ul>
                    </div>  

                <div class="achievements">

                <?php if($imgIDs){ ?>

                 <ul>
                     <?php foreach($imgIDs as $ids){?>
                      <li>
                        <img src="<?php echo wp_get_attachment_url($ids);?>" alt="" />
                      </li>

                     <?php }; ?>
                 </ul>

                <?php }; ?>
                    
                </div>

                </div>              
            </div>
        </div>
    </div>
</section><!-- upcoming events end-->   
<?php return ob_get_clean();
 }

add_shortcode('get_in_touch_form','get_in_touch_form');
function get_in_touch_form($atts,$content,$tag){
    $att = shortcode_atts(array(
        'heading' => '',
        'subhead'=>'',
        'desc' => '',
        'paypal'=>'',
        'paypaldesc'=>'',
        ),$atts);
        ob_start();
?>
    <!-- contact form -->
<section class="upcoming-events contact-form-outer">
    <div class="container">
        <div class="upcoming-events-inner">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="upcoming-heading"><?php if(!empty($att['heading'])){ echo $att['heading'];} ?><span><?php echo $att['subhead'] ?></span>  </h3>

                    <p class="cpara"><?php echo $att['desc'] ?></p>
                </div>
            </div> 
            
            <div class="contact-form-main">
                <div class="row">
                    <div class="col-sm-6 padding-right">
                        <div class="form-inner ">
                            <div class="form-para">
                                <p><?php echo get_field('form_small_heading',get_the_ID()); ?></p>
                            </div>
                                <?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1" ]') ?>
                        </div>
                        
                    </div>
                    <div class="col-sm-6 padding-left">
                        <div class="newsletter-main" style="background-image:url(<?php echo site_url(); ?>/wp-content/uploads/2017/09/newsletter-bg.jpg)">
                            <div class="newsletter">
                                <?php dynamic_sidebar('newsletter'); ?> 
                            </div>
                            <div class="paypal">
                              <p><?php echo $att['paypaldesc']; ?> <a href="<?php echo $att['paypal']; ?>" target="_blank">PayPal Secure Payment</a></p>
                            </div>
                            <p>  Call office for other payment options to avoid card fee</p>
                        </div>
                        
                    </div>
                </div>
            </div>
            

        </div>
    </div>
</section>  <!-- contact form end -->
<?php
return ob_get_clean();
} 


add_shortcode('home_page_slider','home_page_slider');
function home_page_slider($atts,$content,$tag){
    $att = shortcode_atts(array(
        'home_slider' => '',
        ),$atts);
        ob_start();
?>
<!-- Slider start-->
<section class="slider">
    <div class="banner">        
    <?php
    $my_query = new WP_Query('post_type=slider & showposts=-1&order=asc');
    while ($my_query->have_posts()) : $my_query->the_post();
    $post_id=$post->ID;
    $url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
    $imagecat_id = get_post_thumbnail_id($postid2);
    $imagecat_url = wp_get_attachment_image_src($imagecat_id,'full');
    $imagecat_url1 = $imagecat_url[0];
    $post_content=$post->post_content;
    $class = get_post_meta( $post_id,"other_class", true );
    ?>
    <div class="listing">                                       
        <div class="home_slider" style="background-image: url('<?php the_field('slide_image') ?>') ">
            <div class="container">
                <div class="row">
                    <div class="slider_content text-center">
                        <h3> <?php the_field('slide_text') ?> </h3>
                    </div>
                </div>
            </div>
        </div>                              
    </div>
    <?php endwhile; wp_reset_query(); ?>
  </div>
</section> <!-- Slider end-->
<?php
return ob_get_clean();
}


add_shortcode('testimonial_list','testimonial_list');
function testimonial_list($atts,$content,$tag){
    $att = shortcode_atts(array(
        'show_testimonial' => '',
        ),$atts);
        ob_start();
?>
<!-- Testimonial start-->
<section class="testimonial">
    <div class="container">
        <div class=" row">
            <div class="col-sm-12">
                <div class="rating-main">   
                    <ul>                
                        <?php
                        $my_query = new WP_Query('post_type=testimonial & showposts=-1&order=asc');
                        while ($my_query->have_posts()) : $my_query->the_post();
                        $post_id=$post->ID;
                        $url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
                        $imagecat_id = get_post_thumbnail_id($postid2);
                        $imagecat_url = wp_get_attachment_image_src($imagecat_id,'full');
                        $imagecat_url1 = $imagecat_url[0];
                        $post_content=$post->post_content;
                        $class = get_post_meta( $post_id,"other_class", true );
                        ?>
                        <li>
                        <div class="rating">
                            <img src="<?php echo get_field('star') ;?>">
                            <p><?php echo get_field('message'); ?> <span><?php echo get_field('author_name'); ?></span></p>
                        </div>
                        </li>
                    <?php endwhile; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section><!-- Testimonial end-->
<?php
return ob_get_clean();
}

add_shortcode('our_class_room','our_class_room');
function our_class_room($atts,$content,$tag){
    $attr = shortcode_atts(array(
        'heading' => '',
        'url'=>'',
        'color'=>'',
        ),$atts);
        ob_start();
?>
<!-- Classes start-->
<section class="classes" style="background-color:<?php echo $attr['color']; ?>;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12"> 
                <div class="heading-main">
                 <h3><?php echo $attr['heading']; ?></h3>
                </div>
                <div class="classes-type">       
                 <?php
                    $my_query = new WP_Query('post_type=product & showposts=-1&order=asc');
                    while ($my_query->have_posts()) : $my_query->the_post();
                    $post_id=$post->ID;
                    $url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
                    $imagecat_id = get_post_thumbnail_id($postid2);
                    $imagecat_url = wp_get_attachment_image_src($imagecat_id,'full');
                    $imagecat_url1 = $imagecat_url[0];
                    $post_content=$post->post_content;
                    $class = get_post_meta( $post_id,"other_class", true );
                    ?>
                    <div class="listing">                                       
                    <div class="class-inner">
                    <div class="img-box">
                    <!-- <img src="<?php echo get_field('class_image');?>"> -->
                    <img src="<?php echo get_the_post_thumbnail($post_id);?>">
                   
                    </div>
                    <div class="content-box">
                    <span class="days">
                    <?php echo get_field('days'); ?>
                    </span>
                    <h3><?php echo get_the_title($post_id);?>
                    </h3>
                    
                    <?php echo the_excerpt(); ?>
                    
                    
                    <a href="<?php echo get_permalink(); ?>">Get full class information</a>
                    </div>
                    </div>                                  
                    </div>
                <?php endwhile; wp_reset_query(); ?>
             </div>
            <div class="viewAll">
            <a href="<?php echo $attr['url']; ?>" class="viewBtn" target="_blank">View all classes</a>
           </div>
        </div>
    </div>
    </div>
</section><!-- Classes end-->
<?php
return ob_get_clean();
}

add_shortcode('about_flip4me','about_flip4me');
function about_flip4me($atts,$content,$tag){
    $attr = shortcode_atts(array(
        'icon' => '',
        'headingtext'=>'',
        'abouttext'=>'',
        'desc'=>'',
        'learnmore'=>'',
        'iconright'=>'',
        'headingright'=>'',
        'aboutright'=>'',
        'descright'=>'',
        'learnmoreright'=>'',
        ),$atts);
        ob_start();

?>

<!-- About-flip start-->
<section class="about-flip">
    <div class="container-fluid">
        <div class="row">
        <div class="col-sm-6 welcome-flip eql_height">
            <div class="welcome-flip-inner">
                <h3>
                 <span class="heading-icon"><img src="<?php echo wp_get_attachment_url($attr['icon']);?>"> </span>
                    <?php echo $attr['headingtext']; ?>
                    <span class="heading-about"><?php echo $attr['abouttext']; ?></span>
                 </h3>
                 <p><?php echo $attr['desc']; ?></p>
                 <div class="learn-more">
                    <a href="<?php echo $attr['learnmore']; ?>" target="_blank" class="viewBtn">Learn More</a>
                 </div> 
            </div>
        </div>
        <div class="col-sm-6 facilities eql_height" style="background-image: url(<?php echo site_url(); ?>/wp-content/uploads/2017/09/facilities-bg.jpg) ">
            <div class="facilities-offers">
                <h3>
                 <span class="heading-icon"><img src="<?php echo wp_get_attachment_url($attr['iconright']);?>"> </span>
                    <?php echo $attr['headingright']; ?>
                    <span class="heading-about"><?php echo $attr['aboutright']; ?></span>
                 </h3>
                 <p><?php echo $attr['descright']; ?></p>
                 <div class="learn-more">
                    <a href="<?php echo $attr['learnmoreright']; ?>" target="_blank" class="viewBtn">View All</a>
                 </div> 
            </div>
         </div>
        </div>
    </div>
</section><!-- About-flip end-->
<?php
return ob_get_clean();
}